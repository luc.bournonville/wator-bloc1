Le fichier à considérer en premier est wator.py, qui reprend les consignes du tp.
Le fichier wator_tkinter ajoute une animation avec des images, à réserver à des grilles de tailles modestes (<15), idem pour le nombre de cycles.
Le fichier essai_encap.py reprend la définition de l'objet Poisson utilisé dans les deux autres programmes mais complètement encapsulé, 
il conviendrait ensuite d'adapter le reste du programme wator pour utiliser les accesseurs et mutateurs déclarés dans ce fichier.