import matplotlib.pyplot as plt
from random import randint,sample
class Poisson:
    e,gr,gt=3,5,2
    def __init__(self,espece,x,y):
        
        self.__espece=espece
        if self.__espece=="requin":
            self.__gestation = Poisson.gr
        else:
            self.__gestation=Poisson.gt
        self.__x=x
        self.__y=y
        self.__energie=Poisson.e
        
    def get_espece(self):
        return self.__espece
    def get_gestation(self):
        return self.__gestation
    def get_x(self):
        return self.__x
    def get_y(self):
        return self.__y
    
    def set_espece(self,espece):
        self.__espece=espece
    def set_gestation(self,gestation):
        self.__gestation=gestation
    def set_x(self, x):
        self.__x = x
    def set_y(self, y):
        self.__y = y

    def get_energie(self):
        return self.__energie
    
    def set_energie(self):
        self.__energie=Poisson.e

    def duree_gestation(self):
        if self.__gestation>=1:
            self.__gestation-=1
            
    def reset_gestation(self):
        if self.__espece=='thon':
            self.__gestation=Poisson.gt
        else:
            self.__gestation=Poisson.gr
            
    def niveau_energie(self):
        self.__energie-=1
    def reset_energie(self):
        self.__energie=Poisson.e
            
    def bouge(self,x,y): 
        self.__x=x
        self.__=y
        
