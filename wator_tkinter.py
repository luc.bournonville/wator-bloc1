from random import randint, sample
import matplotlib.pyplot as plt
from tkinter import *

class Poisson:
    e,gr,gt=3,5,2
    #constructeur
    def __init__(self,espece,x,y):
        
        self.espece=espece
        if self.espece=="requin":
            self.gestation = Poisson.gr
            self.energie = Poisson.e
        else:
            self.gestation = Poisson.gt

        self.x=x
        self.y=y
    #methodes
    def duree_gestation(self):
        if self.gestation>=1:
            self.gestation-=1
    def reset_gestation(self):
        if self.espece=='thon':
            self.gestation=Poisson.gt
        else:
            self.gestation=Poisson.gr        
    def niveau_energie(self):
        if self.energie>=1:
            self.energie-=1
    def reset_energie(self):
        self.energie=Poisson.e
            
    def bouge(self,x,y): 
        self.x=x
        self.y=y

def creer_grille(n,p):
    """
    Renvoie une tableau à n lignes et p colonnes
    :param: n,p (int)
    :return: L (list)
    >>> creer_grille(3,2)
    [[0, 0], [0, 0], [0, 0]]
    >>>
    """
    L=[]
    for _ in range(n):
        L.append([0]*p)
    return L

def dimension(grille):
    """
    renvoie nombre de lignes et de colonnes
    :param: grille (list)
    :return: (tuple)
    >>> dimension(creer_grille(5,6))
    (5, 6)
    """
    return (len(grille),len(grille[0]))

def creer_liste_point(grille):
    """
    renvoie une liste de tuples à partir d'un tableau à deux dimensions
    :param: grille(list)
    :return: liste_point(list)
    >>> creer_liste_point(creer_grille(3,2))
    [(0, 0), (1, 0), (2, 0), (0, 1), (1, 1), (2, 1)]
    """
    (nb_lignes,nb_colonnes)=dimension(grille)
    liste_point=[]
    for i in range(nb_colonnes):
        for j in range(nb_lignes):
            liste_point.append((j,i))
    return liste_point

def choisir_case_sur_grille(grille):
    """
    renvoie une case au hasard de la grille
    :param:grille(list)
    :return:(x,y) (tuple)
    >>> choisir_case_sur_grille(grille)
    (2, 0)
    >>> choisir_case_sur_grille(grille)
    (2, 2)
    """
    (nb_lignes,nb_colonnes)=dimension(grille)
    x=randint(0,nb_lignes-1)
    y=randint(0,nb_colonnes-1)
    return (x,y)

def verifier_case_vide(grille,x,y):
    """
    prédicat pour teste sur une case de grille est vide de poissons
    c'est à dire contient 0
    :param: grille(list); x,y (int)
    :return: (bool)
    >>> verifier_case_vide(grille,2,1)
    True
    """
    if grille[x][y]==0:
        return True 
    else: 
        return False

def case_de_destination(grille,x,y,direction): 
    """
    applique un changement de direction à un case en 
    utilisant le principe de la grille torique
    :param:grille(list); x,y (int); direction (str)
    :return:destination (tuple)
    >>> case_de_destination(grille,2,1,'E')
    (2, 2)
    >>> case_de_destination(grille,2,2,'E')
    (2, 0)
    >>> 
    """
    (nb_lignes,nb_colonnes)=dimension(grille)
    if direction=="O":
        destination=(x,(y-1)%nb_colonnes)            
    elif direction=="E":
        destination=(x,(y+1)%nb_colonnes)
    elif direction=="N":
        destination=((x-1)%nb_lignes,y)
    else:
        destination=((x+1)%nb_lignes,y)
    return destination

def trouver_case(grille,x,y,verifier):
    '''
    renvoie la liste des cases contigues à la case x,y pour
    lesquelles la fonction verifier renvoie True
    :param:grille(list); x,y (int); verifier(function)
    :return:destination_possible(list)
    >>> trouver_case_vide(grille,1,1,verifie_case_vide)
    [(0, 1), (1, 0)]
    '''
    L=['N','S','E','O']
    destination_possible=[]   
    for direction in L:
        (x1,y1)=case_de_destination(grille,x,y,direction)
        case=verifier(grille,x1,y1)
        if case and not((x1,y1) in destination_possible):
            destination_possible.append((x1,y1))
    return destination_possible

def tirer_case_au_hasard(destination_possible):
    """
    tire un tuple au hasard dans destination_possible
    :param:destination_possible(list)
    :return:destination_tiree(tuple)
    >>> tirer_case_au_hasard(destination_possible)
    (1, 0)
    """
    destination_tiree=destination_possible[randint(0,len(destination_possible)-1)]
    return destination_tiree

def trouver_place_poisson(grille,a,b):
    """
    renvoie une liste de a+b tuples choisis au hasard dans la grille
    :param:grille(list); a,b(int)
    :return:liste_point_occupe(list)
    >>>trouver_place_poisson(grille,1,3)
    [(1, 2), (2, 1), (0, 1), (1, 0)]
    """
    liste_point=creer_liste_point(grille)
    liste_point_occupe=sample(liste_point,a+b)
    return liste_point_occupe

def creer_population(grille,req=5,th=5):
    """
    place des objets Poisson d'espèce requin ou thon sur la grille
    :param:grille(list); req,th(int)
    :return:grille(list)
    :effet de bord:grille
    """
    liste_point_occupe=trouver_place_poisson(grille,req,th)
    for i in range(req):  
        (x,y)=liste_point_occupe[i]
        r=Poisson('requin',x,y)
        grille[x][y]=r
    for i in range(req,req+th):
        point=liste_point_occupe[i]
        (x,y)=point
        t=Poisson('thon',x,y)
        grille[x][y]=t        
    return grille

def afficher_grille(grille):
    """
    prodédure qui affiche la grille ligne par ligne
    :param:grille(list)
    :effet de bord: print 
    >>> afficher_grille(grille)
    O O T T 
    O O O O 
    T T T R 
    """
    (nb_lignes,nb_colonnes)=dimension(grille)
    for i in range(nb_lignes):
        for j in range(nb_colonnes):
            if isinstance(grille[i][j],Poisson):
                if grille[i][j].espece=="thon":
                    print('T',end=' ')
                else:
                    print('R',end=' ')
            else:
                print('O',end=' ')
        print()
        
def vie_du_thon(fish,grille):
    """
    procedure qui gère la vie des thons
    :param:fish (objet Poisson); grille(list)
    :effet de bord : grille
    """
    fish.duree_gestation()
    x=fish.x
    y=fish.y
    destination_possible=trouver_case(grille,x,y,verifier_case_vide)
    if len(destination_possible)!=0:
        (x1,y1)=tirer_case_au_hasard(destination_possible)
        fish.bouge(x1,y1)
        grille[x][y]=0
        grille[fish.x][fish.y]=fish
        if fish.gestation==0:
        # naissance du thon et placement sur sa case     
            t=Poisson('thon',x,y)
            grille[x][y]=t
            
def verifier_case_pleine_thon(grille,x,y):
    """
    prédicat qui valide la présence d'un thon sur la case
    :param:grille(list); x,y(int)
    :return: (bool)
    >>> verifier_case_pleine_thon(grille,0,1)
    False
    >>> verifier_case_pleine_thon(grille,1,0)
    True
    """
    if isinstance(grille[x][y],Poisson): 
        if grille[x][y].espece=="thon":
            return True
        else:
            return False
    else:
        return False
    
def vie_du_requin(fish,grille):
    """
    procédure qui régit la vie des requins
    :param:fish(objet Poisson),grille (list)
    :effet de bord: grille
    """
    fish.niveau_energie()
    fish.duree_gestation()
    x=fish.x
    y=fish.y
    destination_possible=trouver_case(grille,x,y,verifier_case_pleine_thon)
    if len(destination_possible)!=0:
        (x1,y1)=tirer_case_au_hasard(destination_possible)
        (x,y)=(fish.x,fish.y)
        fish.bouge(x1,y1)
        (new_x,new_y)=(fish.x,fish.y)
        grille[x][y]=0
        fish.reset_energie()
        grille[new_x][new_y]=fish
        if fish.gestation==0:
            new_fish=Poisson("requin",x,y)
            grille[x][y]=new_fish
            fish.reset_gestation()
    else:
            
        destination_possible=trouver_case(grille,x,y,verifier_case_vide)
        if len(destination_possible)!=0:
            (x1,y1)=tirer_case_au_hasard(destination_possible)
            (x,y)=(fish.x,fish.y)
            fish.bouge(x1,y1)
            grille[x][y]=0
                
    if fish.niveau_energie()==0:
        del(fish)
        grille[x][y]=0
        
def comportement(grille):
    """
    choisit une case au hasars sur la grille et applique le comportement
    correspondant à l'espèce de poisson placé sur la grille quand celle-ci n'est pas vide.
    :param:grille(list)
    :effet de bord: grille
    """
    (x,y)=choisir_case_sur_grille(grille)
    if isinstance(grille[x][y],Poisson): 
        fish=grille[x][y]
        if fish.espece=="thon":
            vie_du_thon(fish,grille)
        else:
            vie_du_requin(fish,grille)
            
def compter_poisson(grille):
    """
    renvoie le nombre de thons et de requis présents sur la grille
    :param:grille (list)
    :return: nb_requin, nb_thon (int)
    >>> compter_poisson(grille)
    (1, 3)
    """
    (nb_lignes,nb_colonnes)=dimension(grille)
    nb_requin,nb_thon=0,0
    for i in range(nb_colonnes):
        for j in range(nb_lignes):
            if isinstance(grille[j][i],Poisson):
                if grille[j][i].espece=="thon":
                    nb_thon+=1
                else:
                    nb_requin+=1
    return (nb_requin,nb_thon)

def evolution(cycle,ligne, colonne,requin,thon):
    """
    renvoie des listes contenant le nombre de poissons en fonction du nombre de cycles
    :param:cycle,ligne, colonne,requin,thon (int)
    :return:nbReq,nbTh (int)
    >>>evolution(10,3,3,2,3)
    ([2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], [3, 3, 2, 2, 2, 2, 3, 3, 3, 3, 3])
    """
    grille=creer_grille(ligne,colonne)
    creer_population(grille,requin,thon)
    listeX=[]
    nbReq=[requin]
    nbTh=[thon]
    for i in range(cycle):
        comportement(grille)
        r=compter_poisson(grille)
        nbReq.append(r[0])
        nbTh.append(r[1])        
    return (nbReq,nbTh)

def tracer_evolution(cycle=125000,ligne=25, colonne=25,requin=80,thon=160):
    """
    trace les courbes d'
    évolution des populations de
    thons et de requins aux cours des cycles
    :param:cycle,ligne, colonne,requin,thon (int)
    """
    resultat=evolution(cycle,ligne, colonne,requin,thon)
    nbReq=resultat[0]
    nbTh=resultat[1]
    listeX=list(range(0,cycle+1))
    plt.title("Vie des thons et des requins")
    plt.plot(listeX,nbReq,label='requins',color='red')
    plt.plot(listeX,nbTh,label='thons',color='blue')
    plt.xlabel('cycles')
    plt.ylabel('effectifs')
    plt.legend()
    plt.grid()
    plt.show()

    

def mise_a_jour():
    global cpt
    myFen.title(str(cpt))
    for i in range(colonne):
        for j in range(ligne):
            if isinstance(grille[i][j],Poisson):
                if grille[i][j].espece=='thon':
                    imag=im[1]
                else:
                    imag=im[2]
            else:
                imag=im[0]
            canv.create_image(i*75,j*75,image=imag,anchor=NW)
    comportement(grille)
   
    if cpt<cycle:
        cpt+=1
        canv.after(300,mise_a_jour)
    


myFen=Tk()
cycle,ligne, colonne,requin,thon=100,10,10,10,30
grille=creer_grille(ligne,colonne)
creer_population(grille,requin,thon)
cpt=0
myFen.geometry('750x750')
myFen.title('Évolution des populations')
myFen.resizable(width=False,height=False)
canv=Canvas(myFen,width=750,height=750)
canv.place(x=0,y=0)
img_mer=PhotoImage(file='mer.png')
img_thon=PhotoImage(file='thon.png')
img_requin=PhotoImage(file='requin.png')
im=[img_mer,img_thon,img_requin] 

mise_a_jour()
    
myFen.mainloop()
 


 



    
